# PunchClockM223

Project for ÜK M223

# Projekt Starten

Um das Projekt zu starten kann im Terminal von VS Code der Befehl 
./mvnw compile quarkus:dev
ausgeführt werden. 
Wenn die Meldung BUILD SUCCESS erscheint, kann im Browser
http://localhost:8080/
geöffnet und benutzt werden.

# Applikation

Auf der Login-Page kann man sich einen neuen Account erstellen (registrieren) oder sich mit seinem bereits
existierenden Account einloggen. Dafür müssen die Daten in die entsprechenden Felder abgefüllt werden.
In der Applikation hat man nun die Möglichkeit, seine Arbeitszeit zu erfassen, bearbeiten und löschen.
