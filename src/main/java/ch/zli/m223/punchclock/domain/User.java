package ch.zli.m223.punchclock.domain;

import javax.persistence.Column;
import javax.persistence.*;
import javax.persistence.Id;

@Entity
public class User {

    //Im Klassendiagramm = id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userId;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long id) {
        this.userId = id;
    }

    public String getPassword() {
        return password;
    }

/** public void setPassword(String password) {
        this.password = password;
    }
    */

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
