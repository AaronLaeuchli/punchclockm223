package ch.zli.m223.punchclock.domain;


import javax.persistence.*;
import javax.persistence.Id;
import java.util.List;


@Entity
public class Project {

    //Im Klassendiagramm = id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long projectId;

    //Im Klassendiagramm = name
    @Column(nullable = false)
    private String projectName;

    @OneToMany
    private List<Activity> activities;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long id) {
        this.projectId = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String name) {
        this.projectName = name;
    }

    public List<Activity> getActivities() {
        return activities;
    }

    public void setActivities(List<Activity> activities) {
        this.activities = activities;
    }
}