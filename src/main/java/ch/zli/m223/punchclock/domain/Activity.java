package ch.zli.m223.punchclock.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;

@Entity
public class Activity {

    //Im Klassendiagramm = id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long activityId;

    //Im Klassendiagramm = name
    @Column(nullable = false)
    private String activityName;

    private Long projectid;

    public void Add(Activity activity){
        
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long id) {
        this.activityId = id;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String name) {
        this.activityName = name;
    }

    public Long getProjectid() {
        return projectid;
    }

    public void setProjectid(Long projectid) {
        this.projectid = projectid;
    }
    
}
