package ch.zli.m223.punchclock.service;

import ch.zli.m223.punchclock.domain.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;


import java.util.List;

@ApplicationScoped
public class UserService {
    
    @Inject
    EntityManager entityManager;

    public UserService() {
    }

    @Transactional
    public User createUser(User user) {
        List<User> allUser = findAll();
        for (User value : allUser) {
            if (value.getUsername().equals(user.getUsername())) {
                user.setUserId(value.getUserId());
            }
        }
        entityManager.merge(user);
        return user;
    }
    @SuppressWarnings("unchecked")
    public List<User> findAll() {
        var query = entityManager.createQuery("FROM User");
        return query.getResultList();
    }

    @Transactional
    public void deleteUser(Long id){
        User removeUser = getUserById(id);
        entityManager.remove(removeUser);
    }

    @Transactional
    public User getUserById(Long id){
        return entityManager.find(User.class, id);
    };

    @Transactional
    public void update(User user){
        entityManager.merge(user);
    }

    @Transactional
    public User getLoginUser(String username, String password) {
        var query = entityManager.createQuery("FROM User WHERE username = :username and" +
                " password = :password");
            query.setParameter("username", username);
            query.setParameter("password", password);
        return (User) query.getSingleResult();
    }

    @SuppressWarnings("unchecked")
    public List<User> findSpecialUser() {
        var query = entityManager.createQuery("FROM User user " +
                "GROUP BY user.id " +
                "HAVING (user.id) > 5");
        return query.getResultList();
    }

}